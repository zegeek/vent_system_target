/*
 * command.h
 *
 *  Created on: 13 avr. 2020
 *      Author: Zegeek
 */

#ifndef INC_COMMAND_H_
#define INC_COMMAND_H_

#include "main.h"
#include "tools.h"

int8_t interprete_command(const char* cmd);
void send_data();
void _mode_on();
void _mode_off();
void set_ev(const char* label);

#endif /* INC_COMMAND_H_ */
