/*
 * tools.h
 *
 *  Created on: Apr 13, 2020
 *      Author: Zegeek
 */

#ifndef INC_TOOLS_H_
#define INC_TOOLS_H_

#include "string.h"
#include "stdio.h"
#include "math.h"
#include "main.h"

#define BUFFER_SIZE 20
#define SENSOR_FIELD 2
#define IDLE_BLINK 4
#define DATA_BLINK 2

#define N_TO_DO -1
#define ACK 0
#define ALIVE 1
#define DATA 2
#define ENTRY 3
#define WRITE 4
#define START 5
#define STOP 6

#define LABEL 0
#define VALUE 1

typedef struct {
	char label[BUFFER_SIZE];
	char value[BUFFER_SIZE];
}SensorTypedef;
typedef SensorTypedef ActuatorTypedef;

extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart1;
extern TIM_HandleTypeDef htim3;
//private variables

extern char cmdLast[];
extern uint8_t connected;
extern uint8_t sending_data;
extern uint8_t sending_entry;
extern uint8_t reading_entry;
extern uint8_t mode_on;
extern const char* command[];
extern uint16_t blink_activity;
extern SensorTypedef sensors[];
extern ActuatorTypedef actuators[];
extern uint8_t dataSize;
extern uint8_t writeSize;
extern uint8_t dataAnchor;
extern uint8_t dataFieldAnchor;
extern uint16_t adc_buffer;
extern float adcChannel1;

//private functions
void strip_command(char* cmd, const char* cmdBuffer);
void strip_label_value(char *cmd, const char*cmdBuffer);
void format_command(const char* cmd, char* cmdFormat);
uint8_t get_label_index(const char* label, uint8_t data_array);
void update_value(const char* value, SensorTypedef *data_array);
uint16_t get_ev_pin(const char* label);
float sim_pressure_output(uint16_t value);
void ftoa(float value, char str[], int precision);
#endif /* INC_TOOLS_H_ */
