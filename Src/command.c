/*
 * command.c
 *
 *  Created on: 13 avr. 2020
 *      Author: Zegeek
 */

#include "command.h"

int8_t interprete_command(const char* cmd)
{
	char cmdBuffer[BUFFER_SIZE];
	if(strcmp(cmd,command[ALIVE]) == 0)
	{
		if(!connected)
		{
			connected = 1;
			HAL_GPIO_WritePin(CONNECTED_LED_GPIO_Port, CONNECTED_LED_Pin, GPIO_PIN_SET);
			if(mode_on)
				_mode_on();
			else
				_mode_off();
			return ACK;
		}
		else
		{
			return N_TO_DO;
		}
	}
	if(strcmp(cmd,command[DATA]) == 0)
	{
		if(connected)
		{
			if(!sending_data)
			{
				sending_data = 1;
				dataAnchor = 0;
				dataFieldAnchor = 0;
				blink_activity = DATA_BLINK;
				HAL_GPIO_WritePin(DATA_LED_GPIO_Port, DATA_LED_Pin, GPIO_PIN_SET);
				return DATA;
			}
		}
	}
	if(strcmp(cmd,command[ENTRY]) == 0)
	{
		if(connected)
		{
			if(!sending_entry)
			{
				sending_entry = 1;
				dataAnchor = 0;
				dataFieldAnchor = 0;
				blink_activity = DATA_BLINK;
				HAL_GPIO_WritePin(DATA_LED_GPIO_Port, DATA_LED_Pin, GPIO_PIN_SET);
				return ACK;
			}
		}
	}
	if(strcmp(cmd,command[WRITE]) == 0)
	{
		if(connected)
		{
			if(!reading_entry)
			{
				reading_entry = 1;
				dataAnchor = 0;
				dataFieldAnchor = 1;
				blink_activity = DATA_BLINK;
				HAL_GPIO_WritePin(DATA_LED_GPIO_Port, DATA_LED_Pin, GPIO_PIN_SET);
				return ACK;
			}
		}
	}
	if(strcmp(cmd,command[START]) == 0)
	{
		if(connected)
		{
			if(!mode_on)
			{
				mode_on = 1;
				_mode_on();
			}
		}
		return N_TO_DO;
	}
	if(strcmp(cmd,command[STOP]) == 0)
	{
		if(connected)
		{
			if(mode_on)
			{
				mode_on = 0;
				_mode_off();
			}
		}
		return N_TO_DO;
	}
	if(strcmp(cmd,command[ACK]) == 0)
	{
		if(connected)
		{
			if(sending_data)
			{
				if(dataFieldAnchor < SENSOR_FIELD-1)
					dataFieldAnchor++;
				else
				{
					if(dataAnchor < dataSize)
					{
						if(dataAnchor != dataSize - 1)
						{
							dataFieldAnchor = 0;
							dataAnchor++;
						}
						else
						{
							sending_data = 0;
							blink_activity = IDLE_BLINK;
							HAL_GPIO_WritePin(DATA_LED_GPIO_Port, DATA_LED_Pin, GPIO_PIN_RESET);
							return ACK;
						}
					}
					else
					{
						return ACK;
					}
				}
				return DATA;
			}
			if(sending_entry)
			{
				if(dataFieldAnchor < SENSOR_FIELD-1)
					dataFieldAnchor++;
				else
				{
					sending_entry = 0;
					blink_activity = IDLE_BLINK;
					HAL_GPIO_WritePin(DATA_LED_GPIO_Port, DATA_LED_Pin, GPIO_PIN_RESET);
					return ACK;
				}
				return DATA;
			}
			if(reading_entry)
			{
				reading_entry = 0;
				blink_activity = IDLE_BLINK;
				HAL_GPIO_WritePin(DATA_LED_GPIO_Port, DATA_LED_Pin, GPIO_PIN_RESET);
				return N_TO_DO;
			}
		}
	}
	if(cmd[0] == 'l' && cmd[1] == 'l')
	{
		strip_label_value(cmdBuffer, cmd);
		if(sending_entry)
		{
			dataAnchor = get_label_index(cmdBuffer,DATA);
			return DATA;
		}
		if(reading_entry)
		{
			dataAnchor = get_label_index(cmdBuffer, WRITE);
			return ACK;
		}
	}
	if(cmd[0] == 'v' && cmd[1] == 'v')
	{
		strip_label_value(cmdBuffer, cmd);
		if(reading_entry)
		{
			update_value(cmdBuffer, actuators);
			set_ev(actuators[dataAnchor].label);
			return ACK;
		}
	}
	return N_TO_DO;
}
void send_data()
{
	char msg[50] = "";
	char tmp[BUFFER_SIZE]="";
	if(dataAnchor != -1)
	{
		if(dataAnchor == 2 || dataAnchor == 3)
		{
			adcChannel1 = sim_pressure_output(adc_buffer);
			ftoa(adcChannel1,tmp,4);
			strcpy(sensors[2].value,tmp);
			memset(tmp,0,BUFFER_SIZE);
			ftoa(-adcChannel1,tmp,4);
			strcpy(sensors[3].value,tmp);
			memset(tmp,0,BUFFER_SIZE);
			sprintf(msg,"\r%s : %s\n",sensors[3].label,sensors[3].value);
			HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), 5);
		}
		if(dataFieldAnchor == 0)
		{
			format_command(sensors[dataAnchor].label, tmp);
			HAL_UART_Transmit_IT(&huart1, (uint8_t*)(tmp), strlen(tmp));
		}
		else if(dataFieldAnchor == 1)
		{
			format_command(sensors[dataAnchor].value, tmp);
			HAL_UART_Transmit_IT(&huart1, (uint8_t*)(tmp), strlen(tmp));
		}
	}
}
void _mode_on()
{
	HAL_GPIO_WritePin(MODE_ONOFF_GPIO_PORT, MODE_ON_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(MODE_ONOFF_GPIO_PORT, MODE_OFF_Pin, GPIO_PIN_RESET);
}
void _mode_off()
{
	HAL_GPIO_WritePin(MODE_ONOFF_GPIO_PORT, MODE_ON_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MODE_ONOFF_GPIO_PORT, MODE_OFF_Pin, GPIO_PIN_SET);
}
void set_ev(const char* label)
{
	uint8_t index = get_label_index(label, WRITE);
	uint16_t pin = get_ev_pin(label);
	GPIO_PinState state = GPIO_PIN_RESET;
	if(strcmp(actuators[index].value,"1") ==0)
		state = GPIO_PIN_SET;
	HAL_GPIO_WritePin(EV_GPIO, pin, state);
}
