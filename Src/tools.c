/*
 * tools.c
 *
 *  Created on: Apr 13, 2020
 *      Author: Zegeek
 */
#include "tools.h"

void strip_command(char* cmd, const char* cmdBuffer)
{
	//cmd[0] = '\0';
	memset(cmd,0,BUFFER_SIZE);
	strncpy(cmd,cmdBuffer + 1,strlen(cmdBuffer)-2);
}

void strip_label_value(char *cmd, const char*cmdBuffer)
{
	memset(cmd,0,BUFFER_SIZE);
	strncpy(cmd,cmdBuffer+2,strlen(cmdBuffer)-1);
}

void format_command(const char* cmd, char* cmdFormat)
{
	strcpy(cmdFormat,(char[2]){START_CHAR,'\0'});
	strcat(cmdFormat,cmd);
	strcat(cmdFormat,(char[2]){END_CHAR,'\0'});
}

uint8_t get_label_index(const char* label, uint8_t data_array)
{
	uint8_t index = 0;
	uint8_t found = 0;
	//char tmp[BUFFER_SIZE] = "";
	uint8_t maxSize = 0;
	SensorTypedef *__array;
	if(data_array == DATA)
	{
		maxSize = dataSize;
		__array = sensors;
	}
	else if(data_array == WRITE)
	{
		maxSize = writeSize;
		__array = actuators;
	}
	//strncpy(tmp,label,strlen(label));
	while(index < maxSize && !found)
	{
		if(strcmp(__array[index].label,label) == 0)
			found = 1;
		else
			index++;
	}
	if(found)
		return index;
	return -1;
}
void update_value(const char* value, SensorTypedef *data_array)
{
	/*char tmp[BUFFER_SIZE] = "";
	memset(tmp,0,BUFFER_SIZE);
	strncpy(tmp, value,strlen(value)-1);*/
	strcpy(data_array[dataAnchor].value,value);
}
uint16_t get_ev_pin(const char* label)
{
	if(strcmp(label,actuators[0].label) == 0)
		return EV_IN_AIR_Pin;
	else if(strcmp(label,actuators[1].label) == 0)
		return EV_IN_O2_Pin;
	else if(strcmp(label,actuators[2].label) == 0)
		return EV_SEC_Pin;
	else if(strcmp(label,actuators[3].label) == 0)
		return EV_OUT_INS_Pin;
	else if(strcmp(label,actuators[4].label) == 0)
		return EV_OUT_EXP_Pin;
	else
		return 0;
}
float sim_pressure_output(uint16_t value)
{
	float res = (value * 0.0586) - 120;
	res = (res >= 120)? 120:res;
	res = (res <= -120)? -120:res;
	return res;
}
void ftoa(float value, char str[], int precision)
{
	char last[precision+1];
	int intpart = (int)value;
	sprintf(str,"%d.",intpart);
	float fpart = value - (float)intpart;
	fpart = (fpart>0)?fpart:-fpart;
	int fpart_int = fpart * pow(10,precision);
	sprintf(last,"%d",fpart_int);
	strcat(str,last);
}
